<%
    HttpSession sess = request.getSession();
    
    if(sess.getAttribute("sess_username") != null) {
        response.sendRedirect("home.jsp");
    }
    
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h1>Login</h1>
        <form action="home.jsp" method="post">
            Username <input type="text" name="username"/><br/>
            Password <input type="password" name="password"/><br/>
            <input type="submit" value="Login"/>
        </form>
    </body>
</html>
