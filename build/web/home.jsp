<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Enumeration"%>
<%
  HttpSession sess = request.getSession();
  
  if(sess.getAttribute("sess_username") == null) {
      sess.setAttribute("sess_username", request.getParameter("username"));
      session.setAttribute("sess_username", request.getParameter("username"));
      
  }
        out.println(session.getId());
        

    Enumeration attributeNames = sess.getAttributeNames();

    while (attributeNames.hasMoreElements()) {

        String name = (String)attributeNames.nextElement();

        String value = (String)sess.getAttribute(name);

        out.println(name + "=" + value);

    }

%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h1>Home</h1>
        <h2>Hello <%=sess.getAttribute("sess_username")%> </h2>
        
        <%
            ArrayList fruits = new ArrayList();
            fruits.add("Orange");
            fruits.add("Apple");
            pageContext.setAttribute("fruits", fruits);
        %>
        
        <c:forEach var="fruit" items="${fruits}">
            <c:out value="${fruit}" />
        </c:forEach>
        
        <c:forEach var="session" items="${sessionScope}">
            <c:out value="${session.value}" />
            ${session.key}
        </c:forEach>
        
    </body>
</html>
